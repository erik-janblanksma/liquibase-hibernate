package liquibase.ext.hibernate.database;

import liquibase.database.DatabaseConnection;
import org.easymock.EasyMock;
import org.hibernate.cfg.Configuration;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Tests the {@link HibernateDatabase} class.
 */
public class HibernateDatabaseTest {
    private DatabaseConnection conn;
    private HibernateDatabase db;

    @Before
    public void setUp() throws Exception {
        conn = EasyMock.createMock(DatabaseConnection.class);
        db = new HibernateDatabase();
    }

    @After
    public void tearDown() throws Exception {
        db.close();
    }

    @Test
    public void testHibernateUrlSimple() {
        EasyMock.expect(conn.getURL()).andReturn("hibernate:hibernate/Hibernate.cfg.xml");
        EasyMock.replay(conn);

        db.setConnection(conn);
        Configuration config = db.createConfiguration();

        EasyMock.verify(conn);
    }

    @Test
    public void testSpringUrlSimple() {
        EasyMock.expect(conn.getURL()).andReturn("spring:src/test/resources/hibernate/spring.ctx.xml?bean=sessionFactory");
        EasyMock.replay(conn);

        db.setConnection(conn);
        Configuration config = db.createConfiguration();

        EasyMock.verify(conn);
    }


    @Test
    public void testGetConfigType() throws Exception {

    }

    @Test
    public void testGetConfigFile() throws Exception {

    }

    @Test
    public void testSetConnection() throws Exception {

    }

    @Test
    public void testCreateConfiguration() throws Exception {

    }
}
